

void * AnnounceThread(void * ptr) {
	#define ListenPort 40404
	#define BUFLEN 256
	char buf[BUFLEN];
	char Reply[256];
	char MacAddress[32];
	int AncSocket = 0;
	struct sockaddr_in si_me;
	struct sockaddr_in si_other;
	if ((AncSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		MyLog("Unable to open upd socket\n");
		StartupError(100);
		} else {
		MyLog("UDP announce socket opened\n");
	}
	memset((void *) &si_me, 0, sizeof (si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(ListenPort);
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(AncSocket, (const struct sockaddr *) &si_me, sizeof (si_me)) == -1) {
		MyLog("Unable to open bind upd socket to port %i\n", ListenPort);
		} else {
		MyLog("Announce bound to socket %i\n", AncSocket);
	}

	FILE * ReadMac = fopen("/sys/class/net/eth0/address", "r");
	if (ReadMac == NULL) {
		MyLog("Unable to read mac %s\n", strerror(errno));
		sprintf(MacAddress, "00:00:00:00:00:00");
		} else {
		int ReadCount = fread(MacAddress, 1, 17, ReadMac);
		if (ReadCount != 17) {
			sprintf(MacAddress, "00:00:00:00:00:00");
			} else {
			MacAddress[17] = 0; //Null terminate
		}
	}
	Debug("Announce mac %s\n", MacAddress);
	while (1) {
		int slen = sizeof (si_other);
		int ByteCount = recvfrom(AncSocket, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen);
		if (ByteCount == -1) {
			Debug("Announce byte count -1");
		}
		buf[ByteCount] = 0;
		Debug("Received packet from %s:%d\nData: %s\n\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port), buf);
		char * tb = Reply;
		
		pthread_mutex_lock(&namechangemutex);
		{
			tb += sprintf(tb, "%s\r\n%s\r\nPeak = %i", DeviceName, MacAddress, ConvertToPercent(PeakDetector));
		}
		pthread_mutex_unlock(&namechangemutex);
		
		if (sendto(AncSocket, Reply, strlen(Reply), 0, (struct sockaddr *) &si_other, slen) == -1) {
			Debug("Announce packet failure at %i:%i\n", AncSocket, PORT);
			} else {
			Debug("Announce = %s\n", Reply);
		}
	}
}